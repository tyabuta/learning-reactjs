import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import './App.css';

import AceEditor from 'react-ace';
import 'brace/mode/sh';
import 'brace/mode/text';
import 'brace/theme/monokai';

import {TiClipboard, TiPlus, TiMinus, TiDownload, TiCog, TiThMenu, TiTime, TiEdit} from 'react-icons/lib/ti';
import {SortableContainer, SortableElement, SortableHandle, arrayMove} from 'react-sortable-hoc';


class ThreeLineComponent extends Component {
    constructor(props) {
        super(props);
       
        console.log("ThreeLineComponent::constructor");
        console.log(props);
    }

    render() {
        const buttonStyle = {
            fontSize: 24
        };
        return (
            <TiThMenu style={buttonStyle} onClick={this.props.onClick}/>
        );
    }
}

const DragHandle = SortableHandle(ThreeLineComponent);

class TaskComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            note: "",
            hideNote: true,
            showCount: 0,
        };
    }

    toggleTaskNote = () => {
        let hideNote = !this.state.hideNote;
        let showCount = this.state.showCount;
        if (!hideNote) {
            showCount++;
        }

        this.setState({
            hideNote: hideNote,
            showCount: showCount,
        });
    };

    onChangeTaskTitle = (e) => {
        console.log("TaskComponent::onChangeTaskTitle");
        console.log(e.target.value);
        this.setState({title: e.target.value});
    };

    onChangeTaskNote = (text) => {
        console.log("TaskComponent::onChangeTaskNote");
        console.log(text);
        this.setState({note: text});
    };


    onClickHandle = (e) => {
        console.log("TaskComponent::onClickHandle");
        console.log(e);
    };

    onClickNoteExpand = () => {
        this.toggleTaskNote();
    };

    render() {
        let style = {};
        if (this.state.hideNote) {
            style["display"] = "none";
        }
        else if (1 == this.state.showCount) {
            style["height"] = 0;
        }

        return (
            <div>
                <DragHandle onClick={this.onClickHandle}/>
                <input type="text" ref="inputText" value={this.state.title} onChange={this.onChangeTaskTitle}/>
                <TiEdit onClick={this.onClickNoteExpand}/>
                <AceEditor mode="sh"
                           theme="monokai"
                           name="UNIQUE_ID_OF_DIV"
                           tabSize={2}
                           minLines={2}
                           maxLines={Infinity}
                           style={style}
                           value={this.state.note}
                           onChange={this.onChangeTaskNote}
                           editorProps={{$blockScrolling: true}}/>
            </div>
        );
    }
}

const SortableItem = SortableElement(TaskComponent);

const SortableList = SortableContainer(({items}) => {
    return (
        <div>
            {items.map((value, index) => (
                <SortableItem key={`item-${value}`} index={index} value={value}/>
            ))}
        </div>
    );
});

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [0, 1, 2]
        };
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        this.setState({
            items: arrayMove(this.state.items, oldIndex, newIndex),
        });
    };

    onClickPlus = () => {
        console.log("App::onClickPlus");

        let newItem = 1;
        if (0 < this.state.items.length) {
            newItem = Math.max.apply(null, this.state.items) + 1;
        }
        console.log(newItem);


        this.setState({
            items: this.state.items.concat(newItem)
        });
    };

    onClickMinus = () => {
        console.log("App::onClickMinus");
        let items = this.state.items;
        console.log(items)
        items.pop();
        console.log(items)

        this.setState({
            items: items
        });
    };

    render() {
        const buttonStyle = {
            fontSize: 24
        };

        return (
            <div className="App">
                <TiClipboard style={buttonStyle}/>
                <TiDownload style={buttonStyle}/>
                <TiCog style={buttonStyle}/>

                <hr/>

                <TiTime style={buttonStyle}/>
                <TiPlus style={buttonStyle} onClick={this.onClickPlus}/>
                <TiMinus style={buttonStyle} onClick={this.onClickMinus}/>


                <SortableList items={this.state.items} onSortEnd={this.onSortEnd} useDragHandle={true}/>

            </div>
        );
    }
}

export default App;
