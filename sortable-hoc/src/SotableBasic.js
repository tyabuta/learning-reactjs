import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';


class ItemComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {textValue: props.value};
        this.changeText = this.changeText.bind(this);
    }

    changeText(e) {
        this.setState({textValue: e.target.value});
    }

    render() {
        console.log("ItemComponent::render");
        console.log(this.props);
        console.log(this.state);
        return (
            <li>
                <span>{this.props.value}</span>
                <input type="text" ref="inputText" value={this.state.textValue} onChange={this.changeText}/>
            </li>);
    }
}


const SortableItem = SortableElement(ItemComponent);

const SortableList = SortableContainer(({items}) => {
    console.log("SortableList::render");
    console.log(items);

    return (
        <ul>
            {items.map((value, index) => (
                <SortableItem key={`item-${value}`} index={index} valueIndex={index} value={value}/>
            ))}
        </ul>
    );
});

class SortableBasicComponent extends Component {
    constructor(props){
        super(props);

        this.state = {
            items: ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6'],
        };
    }
    onSortEnd = ({oldIndex, newIndex}) => {
        this.setState({
            items: arrayMove(this.state.items, oldIndex, newIndex),
        });
    };
    render() {
        return <SortableList items={this.state.items} onSortEnd={this.onSortEnd}/>;
    }
}

export default SortableBasicComponent;

