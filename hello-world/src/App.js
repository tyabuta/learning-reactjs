import React, {Component } from 'react';
import logo from './logo.svg';
import './App.css';

import AceEditor from 'react-ace';
import 'brace/mode/sh';
import 'brace/mode/text';
import 'brace/theme/monokai';

import { TiClipboard, TiPlus, TiMinus, TiDownload, TiCog, TiThMenu, TiTime } from 'react-icons/lib/ti';


const TaskComponent  = (props) => {
    const buttonStyle = {
        fontSize: 24
    };

    return (
        <div>
            <TiThMenu style={buttonStyle} />
            <input type="text" />

            <AceEditor mode="sh"
                       theme="monokai"
                       name="UNIQUE_ID_OF_DIV"
                       tabSize={2}
                       minLines={2}
                       maxLines={Infinity}
                       editorProps={{$blockScrolling: true}}
                       />
        </div>
    );
};




class App extends Component {
    render() {

        let editors = [];
        for (let i = 0; i < 3; i++) {
            editors.push(
                <AceEditor key={i.toString()}
                           mode="sh"
                           theme="monokai"
                           name="UNIQUE_ID_OF_DIV"
                           tabSize={2}
                           minLines={2}
                           maxLines={Infinity}
                           editorProps={{$blockScrolling: true}}
                />
            );
        }



        const buttonStyle = {
            fontSize: 24
        };

        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>

                <TiClipboard style={buttonStyle} />
                <TiPlus style={buttonStyle} />
                <TiMinus style={buttonStyle} />
                <TiDownload style={buttonStyle} />
                <TiCog style={buttonStyle} />
                <TiThMenu style={buttonStyle} />
                <TiTime style={buttonStyle} />


                {editors}

                <TaskComponent/>
            </div>
        );
    }
}

export default App;
