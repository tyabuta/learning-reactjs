learning-reactjs
================


## create-react-app コマンド

[create-react-appコマンド](http://qiita.com/chibicode/items/8533dd72f1ebaeb4b614)

```bash
npm install -g create-react-app

create-react-app hello-world
```


## React Icons

[https://gorangajic.github.io/react-icons/](https://gorangajic.github.io/react-icons/)

```bash
npm install react-icons --save
```

